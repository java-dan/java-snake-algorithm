package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.AbstractBoard;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.function.Supplier;

public class Board extends AbstractBoard<Elements> {
    private final int START = 1;
    private int[][] board;

    @Override
    public Elements valueOf(char ch) {
        return Elements.valueOf(ch);
    }

    public List<Point> getApples() {
        return get(Elements.GOOD_APPLE);
    }

    @Override
    protected int inversionY(int y) {
        return size - 1 - y;
    }

    public Direction getSnakeDirection() {
        Point head = getHead();
        if (head == null) {
            return null;
        }
        if (isAt(head.getX(), head.getY(), Elements.HEAD_LEFT)) {
            return Direction.LEFT;
        } else if (isAt(head.getX(), head.getY(), Elements.HEAD_RIGHT)) {
            return Direction.RIGHT;
        } else if (isAt(head.getX(), head.getY(), Elements.HEAD_UP)) {
            return Direction.UP;
        } else {
            return Direction.DOWN;
        }
    }

    public Point getHead() {
        List<Point> result = get(
                Elements.HEAD_UP,
                Elements.HEAD_DOWN,
                Elements.HEAD_LEFT,
                Elements.HEAD_RIGHT);
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public List<Point> getBarriers() {
        List<Point> result = getSnake();
        result.addAll(getStones());
        result.addAll(getWalls());
        return result;
    }

    public List<Point> getSnake() {
        Point head = getHead();
        if (head == null) {
            return Arrays.asList();
        }
        List<Point> result = get(
                Elements.TAIL_END_DOWN,
                Elements.TAIL_END_LEFT,
                Elements.TAIL_END_UP,
                Elements.TAIL_END_RIGHT,
                Elements.TAIL_HORIZONTAL,
                Elements.TAIL_VERTICAL,
                Elements.TAIL_LEFT_DOWN,
                Elements.TAIL_LEFT_UP,
                Elements.TAIL_RIGHT_DOWN,
                Elements.TAIL_RIGHT_UP);
        result.add(0, head);
        return result;
    }

    public boolean isGameOver() {
        return getHead() == null;
    }

    @Override
    public String toString() {
        return String.format("Board:\n%s\n" +
                        "Apple at: %s\n" +
                        "Stones at: %s\n" +
                        "Head at: %s\n" +
                        "Snake at: %s\n" +
                        "Current direction: %s",
                boardAsString(),
                getApples(),
                getStones(),
                getHead(),
                getSnake(),
                getSnakeDirection());
    }

    public List<Point> getStones() {
        return get(Elements.BAD_APPLE);
    }

    public List<Point> getWalls() {
        return get(Elements.BREAK);
    }

    private void setPointValue(Point point, int value) {
        board[point.getY()][point.getX()] = value;
    }

    private Supplier<Stream<Point>> deltas() {
        return () -> Stream.of(
                PointImpl.pt(-1, 0), // offset, not a point
                PointImpl.pt(1, 0), // offset, not a point
                PointImpl.pt(0, 1), // offset, not a point
                PointImpl.pt(0, -1) // offset, not a point
        );
    }

    private Stream<Point> neighbours(Point point) {
        return deltas().get().peek(d -> d.change(point));
    }

    private List<Point> neighboursByValue(Point point, int value) {
        return neighbours(point)
                .filter(p -> board[p.getY()][p.getX()] == value)
                .collect(Collectors.toList());
    }

    private boolean isUnvisited(Point point) {
        int x = point.getX();
        int y = point.getY();
        return board[y][x] == 0;
    }

    private Stream<Point> neighboursUnvisited(Point point, Point finish) {
        return neighbours(point)
                .filter(this::isUnvisited)
                .filter(p -> isAt(p, Elements.NONE, getAt(finish)));
    }

    public Optional<List<Point>> trace(Point start, Point finish) {
        int t = size();
        board = new int[t][t];
        int[] counter = {START};
        setPointValue(start, counter[0]);
        counter[0]++;
        boolean found = false;

        Set<Point> points = new HashSet<>();
        points.add(start);
        for(Set<Point> curr = new HashSet<>(points); !(found || curr.isEmpty()); counter[0]++) {
            Set<Point> next = curr.stream()
                    .flatMap(p -> neighboursUnvisited(p, finish))
                    .collect(Collectors.toSet());
            next.forEach(p -> setPointValue(p, counter[0]));

            found = next.contains(finish);
            curr = next;
        }

        if (!found) return Optional.empty();
        LinkedList<Point> path = new LinkedList<>();
        path.add(finish);
        counter[0]--;

        Point curr = finish;
        while (counter[0] > START) {
            counter[0]--;
            Point prev = neighboursByValue(curr, counter[0]).get(0);
            if (prev == start) return Optional.of(path);
            path.addFirst(prev);
            curr = prev;
        }
        return Optional.of(path);
    }
    public Optional<List<Point>> availableNeighbors(Point point) {
        List<Point> directions = neighbours(point)
                .filter(p -> isAt(p, Elements.NONE))
                .collect(Collectors.toList());
        if (directions.size() > 0) return Optional.of(directions);
        return Optional.empty();
    }
    public List<Point> nonWallNeighbours(Point point) {
        return neighbours(point)
                .filter(p -> !isAt(p, Elements.BREAK))
                .collect(Collectors.toList());
    }
}
