package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.RandomDice;

import java.util.List;
import java.util.Optional;

/**
 * User: Elena Mits
 */
public class YourSolver implements Solver<Board> {

    private Dice dice;
    private Board board;

    public YourSolver(Dice dice) {
        this.dice = dice;
    }

    private String getDirections(Point curr, Point next) {
        if(next.getY() > curr.getY()) return Direction.UP.toString();
        if(next.getX() > curr.getX()) return Direction.RIGHT.toString();
        if(next.getY() < curr.getY()) return Direction.DOWN.toString();
        if(next.getX() < curr.getX()) return Direction.LEFT.toString();
        return Direction.UP.toString();
    }

    private String proceedTrace(List<Point> trace) {
            Point curr = trace.get(0);
            Point next = trace.get(1);
            return getDirections(curr, next);
    }

    @Override
    public String get(Board b) {
        board = b;

        List<Point> apples = board.getApples();
        List<Point> stones = board.getStones();
        List<Point> barriers = board.getBarriers();

        Point appleAt = apples.get(0);
        Point stoneAt = stones.get(0);
        Point headAT = board.getHead();

        Optional<List<Point>> traceToApple = board.trace(headAT, appleAt);
        Optional<List<Point>> traceToStone = board.trace(headAT, stoneAt);

        if(traceToApple.isPresent() && board.getSnake().size() < 65) return proceedTrace(traceToApple.get());
        if(traceToStone.isPresent()) return proceedTrace(traceToStone.get());

        Optional<List<Point>> points = board.availableNeighbors(headAT);
        if(points.isPresent()) return getDirections(headAT, points.get().get(0));

        return getDirections(headAT, board.nonWallNeighbours(headAT).get(0));
    }

    public static void main(String[] args) {
        String url = "http://64.226.126.93/codenjoy-contest/board/player/n9lm3wtwdhvzwg9w9q6w?code=120240256121202039";
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
//                "http://codenjoy.com:80/codenjoy-contest/board/player/3edq63tw0bq4w4iem7nb?code=1234567890123456789"
                url,
                new YourSolver(new RandomDice()),
                new Board());
    }

}
